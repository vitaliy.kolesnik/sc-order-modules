package by.softclub.javacource.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@Configuration
@EnableTransactionManagement
@EntityScan(basePackages = {"by.softclub.javacource.entity.domain"})
@ComponentScan(basePackages = {"by.softclub.javacource"})
@EnableJpaRepositories
public class CustomJpaConfiguration {

}
