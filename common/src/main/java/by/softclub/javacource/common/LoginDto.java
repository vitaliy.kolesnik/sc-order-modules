package by.softclub.javacource.common;


import by.softclub.javacource.validator.NameConstraint;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginDto {

    @NameConstraint
    private String login;

    private String password;
}
