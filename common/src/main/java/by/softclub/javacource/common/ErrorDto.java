package by.softclub.javacource.common;

import lombok.Data;

@Data
public class ErrorDto {
    private String errorCode;
    private String errorText;
}
