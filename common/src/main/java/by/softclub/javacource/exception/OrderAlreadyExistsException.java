package by.softclub.javacource.exception;

public class OrderAlreadyExistsException extends RuntimeException {

    public static String error = "ERROR_ORDER_ALREADY_EXISTS";

    public OrderAlreadyExistsException() {
        super(error);
    }

    public OrderAlreadyExistsException(String userError) {
        super(userError);
    }

}
