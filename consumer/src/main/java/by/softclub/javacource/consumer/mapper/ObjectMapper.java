package by.softclub.javacource.consumer.mapper;

public interface ObjectMapper<T1, T2> {
    T2 map(T1 source);
}
