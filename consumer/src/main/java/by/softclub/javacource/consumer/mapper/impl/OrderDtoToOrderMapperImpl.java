package by.softclub.javacource.consumer.mapper.impl;

import by.softclub.javacource.common.OrderDto;
import by.softclub.javacource.consumer.mapper.OrderDtoToOrderMapper;
import by.softclub.javacource.entity.domain.Currency;
import by.softclub.javacource.entity.domain.Order;
import by.softclub.javacource.entity.domain.enums.OrderType;
import by.softclub.javacource.repository.CurrencyRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderDtoToOrderMapperImpl implements OrderDtoToOrderMapper {

    private final CurrencyRepository currencyRepository;

    public OrderDtoToOrderMapperImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public Order map(OrderDto dto) {

        var order = new Order();
        order.setAmount(dto.getAmount());
        order.setFee(dto.getFee());
        order.setOrderType(OrderType.BUY);
        order.setSourceCurrency(getCurrency(dto.getSourceCurrency()));
        order.setTargetCurrency(getCurrency(dto.getTargetCurrency()));

        return order;
    }

    private Currency getCurrency(String iso) {
        Optional<Currency> optCurr = currencyRepository.findByCode(iso);
        if (optCurr.isPresent()) {
            return optCurr.get();
        } else {
            throw new RuntimeException("Currency with code " + iso + " not found");
        }
    }

}
