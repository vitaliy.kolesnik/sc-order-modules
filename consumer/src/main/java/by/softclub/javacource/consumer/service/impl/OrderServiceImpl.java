package by.softclub.javacource.consumer.service.impl;


import by.softclub.javacource.common.OrderDto;
import by.softclub.javacource.consumer.mapper.OrderDtoToOrderMapper;
import by.softclub.javacource.consumer.service.OrderService;
import by.softclub.javacource.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderDtoToOrderMapper orderDtoMapper;
    private final JavaMailSender emailSender;

    public OrderServiceImpl(OrderRepository orderRepository,
                            OrderDtoToOrderMapper orderDtoMapper,
                            @Qualifier("getJavaMailSender") JavaMailSender emailSender) {
        this.orderRepository = orderRepository;
        this.orderDtoMapper = orderDtoMapper;
        this.emailSender = emailSender;
    }

    @Override
    public void create(OrderDto dto) {
        var msg = "";
        if (dto != null) {
            log.info(dto.toString());
            var order = orderDtoMapper.map(dto);
            var id = orderRepository.save(order).getId();
            msg = String.format("Order No.%d successfully created", id);
        } else {
            msg = "Order is not valid and could not create.";
        }
//        sendMail(msg);
    }

    public void sendMail(String msg) {
        var message = new SimpleMailMessage();
        message.setTo("vitaliy.kolesnik@softclub.by");
        message.setSubject("Create Order");
        message.setText(msg);

        emailSender.send(message);
    }

}
