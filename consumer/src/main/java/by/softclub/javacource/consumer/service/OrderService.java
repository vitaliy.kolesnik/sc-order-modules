package by.softclub.javacource.consumer.service;


import by.softclub.javacource.common.OrderDto;

public interface OrderService {

    void create(OrderDto dto);

}
