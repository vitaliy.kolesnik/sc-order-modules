package by.softclub.javacource.consumer.service.impl;

import by.softclub.javacource.common.OrderDto;
import by.softclub.javacource.consumer.service.OrderService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaMsgReceiveServiceImpl {

    private final OrderService orderService;

    public KafkaMsgReceiveServiceImpl(OrderService orderService) {
        this.orderService = orderService;
    }

    @KafkaListener(id = "1234", topics = {"softclub"}, containerFactory = "singleFactory")
    public void consume(OrderDto dto) {
        orderService.create(dto);
    }

}

