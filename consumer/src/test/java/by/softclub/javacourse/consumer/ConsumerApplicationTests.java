package by.softclub.javacourse.consumer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@SpringBootTest
class ConsumerApplicationTests {

    @Qualifier("getJavaMailSender")
    @Autowired
    JavaMailSender emailSender;

    @Test
    void contextLoads() {
    }

    @Test
    void sendMailTest() {
        var msg = "test";
        var message = new SimpleMailMessage();
        message.setTo("vitaliy.kolesnik@softclub.by");
        message.setSubject("Create Order");
        message.setText(msg);

        emailSender.send(message);
    }

}
