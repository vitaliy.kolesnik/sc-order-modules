package by.softclub.javacource.entity.domain.enums;

public enum OrderState {
    CREATED,
    IN_PROCESS,
    FINISHED,
    CANCELED,
    ERROR;
}
