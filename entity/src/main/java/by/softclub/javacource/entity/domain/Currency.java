package by.softclub.javacource.entity.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Immutable
@Entity
@Table(name = "sc_currencies")
@Getter
@Setter
public class Currency {

    @Id
    private int id;

    private String code;

    private String description;

}
