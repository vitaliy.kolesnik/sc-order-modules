package by.softclub.javacource.service;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class TranslationService {
    private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;

    private final MessageSource messageSource;

    public TranslationService(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getLocalMessage(String code, Locale locale) {
        if (locale == null) {
            locale = DEFAULT_LOCALE;
        }
        return messageSource.getMessage(code, null, locale);
    }

}
