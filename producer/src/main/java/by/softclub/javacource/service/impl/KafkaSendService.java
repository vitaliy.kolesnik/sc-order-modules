package by.softclub.javacource.service.impl;


import by.softclub.javacource.common.OrderDto;
import by.softclub.javacource.service.SendService;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSendService implements SendService {

    private final KafkaTemplate<Long, OrderDto> kafkaTemplate;

    public KafkaSendService(KafkaTemplate<Long, OrderDto> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void send(OrderDto dto) {
        kafkaTemplate.send("softclub", dto);
    }

}
