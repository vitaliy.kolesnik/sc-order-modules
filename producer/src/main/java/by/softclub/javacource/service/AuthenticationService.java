package by.softclub.javacource.service;


import by.softclub.javacource.common.LoginDto;
import by.softclub.javacource.entity.domain.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthenticationService extends UserDetailsService {

    void replaceUserInCache(String token, User user);

    String login(User user, LoginDto loginDto);

}
