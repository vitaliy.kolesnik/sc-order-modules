package by.softclub.javacource.service.impl;


import by.softclub.javacource.common.LoginDto;
import by.softclub.javacource.config.jwt.JwtTokenProvider;
import by.softclub.javacource.repository.UserRepository;
import by.softclub.javacource.entity.domain.User;
import by.softclub.javacource.service.AuthenticationService;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Transactional
public class AuthenticationServiceImpl implements AuthenticationService {

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final JwtTokenProvider jwtTokenProvider;

    private final HazelcastInstance hazelcastInstance;

    private static final String CACHE_NAME = "users";

    public AuthenticationServiceImpl(PasswordEncoder passwordEncoder, UserRepository userRepository, JwtTokenProvider jwtTokenProvider, @Qualifier("hazelcastInstance") HazelcastInstance hazelcastInstance) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.jwtTokenProvider = jwtTokenProvider;
        this.hazelcastInstance = hazelcastInstance;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        IMap<Object, Object> map = hazelcastInstance.getMap(CACHE_NAME);
        User user = (User) map.get(s);
        if (user != null) {
            return user;
        } else {
            return userRepository.findByUsernameEquals(s);
        }
    }

    @Override
    public void replaceUserInCache(String token, User user) {
        IMap<Object, Object> map = hazelcastInstance.getMap(CACHE_NAME);
        user.setPreviousToken(token);
        map.put(user.getUsername(), user, 60, TimeUnit.MINUTES);

    }

    @Override
    public String login(User user, LoginDto loginDto) {
        if (!passwordEncoder.matches(loginDto.getPassword(), user.getPassword())) {
            throw new RuntimeException();
        }

        String token = jwtTokenProvider.createToken(user.getUsername(), user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));

        var now = LocalDateTime.now();
        user.setLoginDate(now);
        user.setPreviousToken(token);
        replaceUserInCache(token, user);

        return token;
    }

}
