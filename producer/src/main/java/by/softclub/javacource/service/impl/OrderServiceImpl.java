package by.softclub.javacource.service.impl;


import by.softclub.javacource.common.OrderDto;
import by.softclub.javacource.repository.OrderRepository;
import by.softclub.javacource.mapper.OrderToOrderDtoMapper;
import by.softclub.javacource.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderToOrderDtoMapper orderMapper;

    public OrderServiceImpl(OrderRepository orderRepository, OrderToOrderDtoMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    @Override
    public List<OrderDto> findAll() {
        log.info(SecurityContextHolder.getContext().getAuthentication().toString());
        return orderRepository.findAll().stream()
                .map(orderMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(int id) {
        orderRepository.deleteById(id);
    }
}
