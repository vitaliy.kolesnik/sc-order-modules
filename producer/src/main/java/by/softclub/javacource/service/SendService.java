package by.softclub.javacource.service;


import by.softclub.javacource.common.OrderDto;

public interface SendService {
    void send(OrderDto dto);
}
