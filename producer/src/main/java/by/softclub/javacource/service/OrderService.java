package by.softclub.javacource.service;

import by.softclub.javacource.common.OrderDto;

import java.util.List;

public interface OrderService {

    List<OrderDto> findAll();

    void deleteById(int id);
}
