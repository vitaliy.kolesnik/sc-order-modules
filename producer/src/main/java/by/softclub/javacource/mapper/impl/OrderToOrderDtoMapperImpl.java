package by.softclub.javacource.mapper.impl;


import by.softclub.javacource.common.OrderDto;
import by.softclub.javacource.entity.domain.Order;
import by.softclub.javacource.mapper.OrderToOrderDtoMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderToOrderDtoMapperImpl implements OrderToOrderDtoMapper {

    @Override
    public OrderDto map(Order order) {
        var dto = new OrderDto();
        dto.setAmount(order.getAmount());
        dto.setFee(order.getFee());
        dto.setId(order.getId());
        dto.setSourceCurrency(order.getSourceCurrency().getCode());
        dto.setTargetCurrency(order.getTargetCurrency().getCode());
        return dto;
    }

}
