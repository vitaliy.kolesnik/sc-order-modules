package by.softclub.javacource.mapper;

import by.softclub.javacource.common.OrderDto;
import by.softclub.javacource.entity.domain.Order;

public interface OrderDtoToOrderMapper extends ObjectMapper<OrderDto, Order> {
}
