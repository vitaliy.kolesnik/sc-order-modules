package by.softclub.javacource.controller;

import by.softclub.javacource.common.LoginDto;
import by.softclub.javacource.common.OrderDto;
import by.softclub.javacource.entity.domain.Currency;
import by.softclub.javacource.entity.domain.Order;
import by.softclub.javacource.entity.domain.enums.OrderState;
import by.softclub.javacource.repository.CurrencyRepository;
import by.softclub.javacource.repository.OrderRepository;
import by.softclub.javacource.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    void getOrders() {
    }

    @Test
    void createOrder() {
    }

    @Test
    void deleteOrder() {
    }

    @Test
    void addCurrenciesTest() {
        var currency = new Currency();

        currency.setId(100);
        currency.setCode("TST");
        currency.setDescription("TEST");

        currencyRepository.saveAndFlush(currency);

        currency.setCode("TEST IMMUTABLE");

        currencyRepository.saveAndFlush(currency);
    }

    @Test
    void addOrderTest() {
        var order = new Order();

        currencyRepository.findById(1).ifPresent(
                order::setSourceCurrency
        );
        currencyRepository.findById(2).ifPresent(
                order::setTargetCurrency
        );
        order.setAmount(new BigDecimal("100"));
        order.setFee(new BigDecimal("0.02"));
        order.setOrderState(OrderState.CREATED);
        orderRepository.saveAndFlush(order);

    }

    @Test
    void findOrdersTest() {
        List<OrderDto> dtoList = orderService.findAll();
        Assertions.assertFalse(dtoList.isEmpty());
        System.out.println(dtoList);
    }

    @Test
    void findOrders401Test() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/order")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized()).andReturn();

        String resultStr = result.getResponse().getContentAsString();
        assertNotNull(resultStr);
        System.out.println(resultStr);

    }

    @Test
    void findOrdersAuthTest() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/order")
                .header("Authorization", "Basic YWRtaW46YWRtUGFzcw==")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        String resultStr = result.getResponse().getContentAsString();
        assertNotNull(resultStr);
        System.out.println(resultStr);

    }

    @Test
    void createOrderTest() throws Exception {

        var dto = new OrderDto();
        dto.setTargetCurrency("USD");
        dto.setSourceCurrency("RUB");
        dto.setFee(BigDecimal.valueOf(23.09));
        dto.setAmount(BigDecimal.valueOf(100.67));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/order/create")
                .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwicm9sZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTU5MTU0MjMwMywiZXhwIjoxNTkxNTQ1OTAzfQ.V7qjdzekHD0cWyugQtV0y5TEdp1JkITcvVotZmxpDg0")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

    }

    @Test
    void deleteOrderTest() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.delete("/order/delete/1")
                .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwicm9sZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTU5MTI5MjUxMCwiZXhwIjoxNTkxMjk2MTEwfQ.zEYT6LmQSnPJaSAPBzW9ZDnMn1c6j9pXm61tcZ-n32s")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    void loginTest() throws Exception {
        var dto = new LoginDto();
        dto.setLogin("user");
        dto.setPassword("qwerty");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    void generatePasswordTest() throws Exception {
        String res = passwordEncoder.encode("qwerty");
        System.out.println("password:" + res);
        Assertions.assertTrue(passwordEncoder.matches("qwerty", res));
    }

}
